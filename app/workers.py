import logging
import threading

from lxml import etree

from archivers import ArchiveException

logger = logging.getLogger('')

lock = threading.Lock()
file1_contents = [('id', 'level'), ]
file2_contents = [('id', 'object_name'), ]


class ParseWorker(threading.Thread):
    def __init__(self, archive_path, archiver_class, *args, **kwargs):
        self.archive_path = archive_path
        self.archiver_class = archiver_class
        super().__init__(*args, **kwargs)

    def run(self):
        logger.debug('start worker with name "%s"', self.name)

        while lock.locked():
            continue

        try:
            with self.archiver_class(self.archive_path) as archive_file:
                for name in archive_file.file_list():
                    logger.debug('unzipping file with name "%s"', name)

                    content = archive_file.read_file(name)
                    tree = etree.fromstring(content)

                    var_id_tag = tree.xpath("var[@name='id']")[0]
                    var_level_tag = tree.xpath("var[@name='level']")[0]
                    objects_tag = tree.xpath('objects')[0]

                    logger.debug('exist data for file 1, id: "%s", level: "%s"', var_id_tag.attrib['value'],
                                 var_level_tag.attrib['value'])
                    lock.acquire()
                    logger.debug('lock acquire %s!', self.name)
                    file1_contents.append((var_id_tag.attrib['value'], var_level_tag.attrib['value']))

                    for object_tag in objects_tag.getchildren():
                        logger.debug('exist data for file 2, id: "%s", object_name: "%s"', var_id_tag.attrib['value'],
                                     object_tag.attrib['name'])
                        file2_contents.append((var_id_tag.attrib['value'], object_tag.attrib['name']))
                    lock.release()
                    logger.debug('lock release %s!', self.name)
        except ArchiveException as e:
            logger.warning(*e.args)
