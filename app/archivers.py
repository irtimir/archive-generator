import abc
import zipfile
from abc import abstractmethod


class ArchiveException(Exception):
    pass


class AbstractArchiver(metaclass=abc.ABCMeta):
    def __init__(self, file, mode='r'):
        if self.check_extension(file) is False:
            raise ArchiveException('file with name "%s", extension is not "%s"', file, self.file_extension)

        self.archive = None
        self._init_archive(file, mode)

    def __enter__(self):
        return self

    @classmethod
    def check_extension(cls, name):
        if isinstance(name, str):
            if name.endswith(cls.file_extension):
                return True
            return False

        return None

    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError

    @abstractmethod
    def _init_archive(self, file, mode='r'):
        raise NotImplementedError

    @abstractmethod
    def add_file(self, file_name, content):
        raise NotImplementedError

    @abstractmethod
    def read_file(self, name, pwd):
        raise NotImplementedError

    @abstractmethod
    def file_list(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def file_extension(self):
        raise NotImplementedError


class ZipArchiver(AbstractArchiver):
    archive_base_class = zipfile.ZipFile
    file_extension = '.zip'

    def _init_archive(self, file, mode='r'):
        self.archive = self.archive_base_class(file, mode=mode, compression=zipfile.ZIP_DEFLATED)

    def add_file(self, file_name, content):
        self.archive.writestr(file_name, content)

    def file_list(self):
        return self.archive.namelist()

    def read_file(self, name, pwd=None):
        return self.archive.read(name, pwd)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.archive.close()
