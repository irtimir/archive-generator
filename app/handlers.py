import csv
import io
import logging
import os

from creators import XmlCreator
from helpers import action_receiver, receiver, ARCHIVERS_MAP
from workers import ParseWorker, file1_contents, file2_contents

logger = logging.getLogger('')


@receiver('generate', action_receiver)
def generate_handler(arguments):
    logger.debug('start "generate" action')
    if not os.path.exists(arguments.directory):
        logger.info('directory "%s" does not exists, creating', arguments.directory)
        os.mkdir(arguments.directory)

    archiver_class = ARCHIVERS_MAP[arguments.compressor]

    for n in range(arguments.number_of_archives):
        mf = io.BytesIO()
        archive_name = str(n) + archiver_class.file_extension
        logger.debug('start creating archive with name "%s"', archive_name)

        with archiver_class(mf, mode='w') as archive_file:
            for _ in range(arguments.number_of_xmls):
                creator = XmlCreator()
                creator.create()
                logger.debug('writing in BytesIO file "%s", content "%s"', creator.file_name, creator.string[:50])
                archive_file.add_file(creator.file_name, creator.string)

        with open(os.path.join(arguments.directory, archive_name), 'wb') as f:
            logger.debug('write data in "%s"', archive_name)
            f.write(mf.getvalue())

        logger.debug('archive with name "%s" successfully created', archive_name)

    logger.info('successfully created %s archives with %s xml-files in "%s"', arguments.number_of_archives,
                arguments.number_of_xmls, arguments.directory)


@receiver('parse', action_receiver)
def parse_handler(arguments):
    threads = []

    archiver_class = ARCHIVERS_MAP[arguments.compressor]

    for archive in os.listdir(arguments.directory):
        pw = ParseWorker(os.path.join(arguments.directory, archive), archiver_class)
        threads.append(pw)
        pw.start()

    [thread.join() for thread in threads]

    with open(os.path.join(arguments.directory, '1.csv'), 'w') as f:
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        logger.debug('writing data for file 1')
        wr.writerows(file1_contents)

    with open(os.path.join(arguments.directory, '2.csv'), 'w') as f:
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        logger.debug('writing data for file 2')
        wr.writerows(file2_contents)

    logger.info('data successfully parsed, for file 1 exist %s elements, for file 2 exist %s elements',
                len(file1_contents) - 1, len(file2_contents) - 1)
