import argparse
import logging.config
import time

from helpers import action_receiver

__import__('handlers')

logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(levelname)-8s %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
        'brief': {
            'format': '%(levelname)-8s %(asctime)s %(name)-16s %(message)s',
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        '': {
            'level': 'INFO',
            'propagate': True,
            'handlers': ['console', ],
        },
    }
})

logger = logging.getLogger('')


def main():
    start_time = time.time()
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--action', help='action for script', type=str, default='generate',
                        choices=['generate', 'parse', ])
    parser.add_argument('-d', '--directory', help='directory for working', default='./tmp', type=str)
    parser.add_argument('-n', '--number-of-archives', help='number of archives', default=50, type=int)
    parser.add_argument('-e', '--number-of-xmls', help='number of xml\'s', default=100, type=int)
    parser.add_argument('-c', '--compressor', help='compressor type', default='zip', type=str, choices=['zip', ])

    args = parser.parse_known_args()

    logger.debug('settings arguments: %s', args[0])
    logger.info('start execution "%s" action', args[0].action)

    action_receiver.send(args[0].action, args[0])

    logger.info('execution time: %s seconds', time.time() - start_time)


if __name__ == '__main__':
    main()
