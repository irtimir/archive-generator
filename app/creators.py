import random
import time

from lxml import etree

from helpers import get_random_string


class XmlCreator(object):
    def __init__(self, var_level_range=(1, 101), max_objects_count=10):
        self.context = {
            'var_level_range': var_level_range,
            'max_objects_count': max_objects_count,
        }
        self.string = None
        self.file_name = None

    def create(self):
        """
        :return: bytes string with xml
        :rtype: bytes
        """
        root = etree.Element('root')

        unique_str = str(hash(time.time()))

        self.file_name = unique_str + '.xml'

        etree.SubElement(root, 'var', attrib={'name': 'id', 'value': unique_str})
        etree.SubElement(root, 'var',
                         attrib={'name': 'level', 'value': str(random.randint(*self.context['var_level_range']))})
        objects = etree.SubElement(root, 'objects')

        for _ in range(random.randint(1, self.context['max_objects_count'])):
            etree.SubElement(objects, 'object', attrib={'name': get_random_string()})

        self.string = etree.tostring(root, xml_declaration=True, encoding='utf-8')

        return self.string
