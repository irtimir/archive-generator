import random
import string
from collections import defaultdict

from archivers import ZipArchiver

ARCHIVERS_MAP = {
    'zip': ZipArchiver,
}


def get_random_string(size=6, chars=string.ascii_uppercase + string.digits):  # pragma: no cover
    return ''.join(random.choice(chars) for _ in range(size))


class ActionReceiver(object):  # pragma: no cover
    def __init__(self):
        self.receivers = defaultdict(list)

    def connect(self, receiver_func, method_name):
        self.receivers[method_name].append(receiver_func)

    def send(self, event: dict, *args, **kwargs):
        receivers_list = self.receivers.get(event, [])
        for rec in receivers_list:
            rec(*args, **kwargs)


def receiver(method_name: str, sender):  # pragma: no cover
    def _decorator(func):
        sender.connect(func, method_name)
        return func

    return _decorator


action_receiver = ActionReceiver()
