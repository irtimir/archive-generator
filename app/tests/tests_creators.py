import logging
from unittest import TestCase

from lxml import etree

from creators import XmlCreator

logging.disable(logging.CRITICAL)


class TestCreators(TestCase):
    def setUp(self):
        self.creator = XmlCreator()
        self.creator.create()

    def test_xml_creator_type(self):
        self.assertIsInstance(self.creator.string, bytes)

    def test_xml_creator_structure(self):
        tree = etree.fromstring(self.creator.string)

        var_id_tag = tree.xpath("var[@name='id']")
        var_level_tag = tree.xpath("var[@name='level']")
        objects_tag = tree.xpath('objects')

        self.assertEqual(len(var_id_tag), 1)
        self.assertEqual(len(var_level_tag), 1)
        self.assertEqual(len(objects_tag), 1)

        var_id_tag = var_id_tag[0]
        var_level_tag = var_level_tag[0]
        objects_tag = objects_tag[0]
        objects_tags = objects_tag.getchildren()

        self.assertGreater(len(var_id_tag.attrib['value']), 0)
        self.assertTrue(var_level_tag.attrib['value'].isdigit())
        self.assertGreater(len(objects_tags), 0)
        self.assertLess(len(objects_tags), 11)

        object_tag = objects_tags[0]

        self.assertGreater(len(object_tag.attrib['name']), 0)
