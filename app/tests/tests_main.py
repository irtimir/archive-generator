import argparse
import logging
import os
import shutil
import zipfile
from unittest import mock, TestCase

from main import main

logging.disable(logging.CRITICAL)


class TestMain(TestCase):
    @mock.patch('argparse.ArgumentParser.parse_known_args')
    def test_generate_data_structure(self, mock_parse_known_args):
        directory = './for_test_data'
        number_of_archives = 10
        number_of_xmls = 100

        mock_parse_known_args.return_value = (
            argparse.Namespace(action='generate', number_of_archives=number_of_archives, number_of_xmls=number_of_xmls,
                               directory=directory, compressor='zip'), [])
        main()

        zf = zipfile.ZipFile(os.path.join(directory, os.listdir(directory)[0]))

        self.assertEqual(len(zf.namelist()), number_of_xmls)
        self.assertEqual(len(os.listdir(directory)), number_of_archives)

        shutil.rmtree(directory)

    @mock.patch('argparse.ArgumentParser.parse_known_args')
    def test_parse_data(self, mock_parse_known_args):
        directory = './tests/for_parse_data'
        number_of_archives = 10
        number_of_xmls = 100

        mock_parse_known_args.return_value = (
            argparse.Namespace(action='parse', number_of_archives=number_of_archives, number_of_xmls=number_of_xmls,
                               directory=directory, compressor='zip'), [])
        main()

        file1_path = os.path.join(directory, '1.csv')
        file2_path = os.path.join(directory, '2.csv')

        self.assertTrue(os.path.exists(file1_path))
        self.assertTrue(os.path.exists(file2_path))

        with open(file1_path, 'r') as f:
            file1_lines_count = len(f.readlines())

        self.assertEqual(file1_lines_count - 1, number_of_archives * number_of_xmls)

        with open(file2_path, 'r') as f:
            file2_lines_count = len(f.readlines())

        self.assertGreaterEqual(file2_lines_count - 1, number_of_archives * number_of_xmls)
        self.assertLessEqual(file2_lines_count - 1, number_of_archives * number_of_xmls * 10)

        os.remove(file1_path)
        os.remove(file2_path)
