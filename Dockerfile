FROM python:3.5
COPY ./app .
RUN pip install -r requirements.txt
CMD make test